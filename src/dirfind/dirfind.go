package main

import (
    "os"
    "fmt"
    "io/ioutil"
    "strings"
)


func traverse(args ...string) {
    // fmt.Println(args[0]);

    files, _ := ioutil.ReadDir(args[0])

    if len(files) == 0 {
        return
    }


    for _, f := range files {
        // fmt.Println(args[0] + "/" + f.Name())
        if f.IsDir() {
            // fmt.Println(strings.Contains(f.Name(), args[1]))
            if strings.Contains(f.Name(), args[1])  {
                fmt.Println("Found: " + args[0] + "/" + f.Name())
            }

            traverse(args[0] + "/" + f.Name(), args[1])
        }
    }
}

func main() {
    if (len(os.Args) > 1) {
        search := os.Args[1]
        traverse(".", search)
    } else {
        fmt.Println("Usage ./dirfind SEARCH");
    }
}
